package com.wissionsampleapp.data;

import java.util.List;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public interface DataManager {

    void loginWithFirebase(String email, String password, Callback<Boolean> callback);
    void destroy();



    /**
     * Generic callback interface for passing response to caller.
     *
     * @param <T> Type of expected response
     */
    interface Callback<T> {
        /**
         * Gets invoked when call was successful
         *
         * @param result result of the operation
         */
        void onResponse(T result);

        /**
         * Gets invoked when there is an error in the operation
         */
        void onError();
    }
}