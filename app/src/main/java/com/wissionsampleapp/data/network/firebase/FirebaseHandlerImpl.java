package com.wissionsampleapp.data.network.firebase;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wissionsampleapp.data.DataManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@Singleton
public class FirebaseHandlerImpl implements FirebaseHandler {

    private FirebaseAuth mFirebaseAuth;
    //KEYS here
    private static final String KEY_SLACK_HANDLE = "wission-handle";
    private static final String KEY_USER_NAME = "name";
    private static final String KEY_USER_PIC = "image";
    private static final String KEY_USER_EMAIL = "email";

    private static final String KEY_TIMESTAMP = "timestamp";

    private DatabaseReference mUsersRef;
    private DatabaseReference mQuizzesRef;
    private DatabaseReference mDiscussionsRef;
    private DatabaseReference mResourcesRef;

    private List<ValueEventListener> mValueListeners;

    // Private variables
    private FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

    private static FirebaseHandlerImpl sInstance = null;

    @Inject
    public FirebaseHandlerImpl() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference rootRef = firebaseDatabase.getReference();

        mValueListeners = new ArrayList<>();

        mUsersRef = rootRef.child(REF_USERS_NODE);
       /* mQuizzesRef = rootRef.child(REF_QUIZZES_NODE);
        mDiscussionsRef = rootRef.child(REF_DISCUSSION_NODE);
        mResourcesRef = rootRef.child(REF_RESOURCES_NODE);*/
    }

    public static FirebaseHandlerImpl getInstance() {
        if (sInstance == null) {
            synchronized (FirebaseHandlerImpl.class) {
                if (sInstance == null) {
                    sInstance = new FirebaseHandlerImpl();
                }
            }
        }
        return sInstance;
    }


    @Override
    public void loginWithFirebase(String email, String password, final DataManager.Callback<Boolean> callback) {
        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        callback.onResponse(true);
                    } else {
                        callback.onResponse(false);
                    }
                })
                .addOnFailureListener(e -> {
                    callback.onError();
                });
    }

    @Override
    public void destroy() {
        // Remove all listeners
        for (ValueEventListener listener : mValueListeners) {
            mUsersRef.removeEventListener(listener);

           /* mQuizzesRef.removeEventListener(listener);
            mDiscussionsRef.removeEventListener(listener);
            mDiscussionsRef.removeEventListener(listener);*/
        }
    }
}