package com.wissionsampleapp.data.network.models;

/**
 * Created by Shivam Sharma on 31-12-2018.
 */
public class Post {
    private String vedioUrl;
    private String title;
    private String viewCount;

    public Post() {
    }

    public Post(String vedioUrl, String title, String viewCount) {
        this.vedioUrl = vedioUrl;
        this.title = title;
        this.viewCount = viewCount;
    }

    public String getVedioUrl() {
        return vedioUrl;
    }

    public void setVedioUrl(String vedioUrl) {
        this.vedioUrl = vedioUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }
}
