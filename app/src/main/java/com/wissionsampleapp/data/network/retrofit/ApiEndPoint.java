package com.wissionsampleapp.data.network.retrofit;

import com.wissionsampleapp.BuildConfig;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public final class ApiEndPoint {

    public static final String ENDPOINT_YOUTUBE_SEARCH = BuildConfig.BASE_URL
            + "/search?part=snippet&type=video&videoEmbeddable=true&order=viewCount&q=stand%20up%20comedy&maxResults=10&key=" +BuildConfig.API_KEY ;

    public static final String ENDPOINT_YOUTUBE_ANALYTICS = BuildConfig.BASE_URL
            + "/search?part=snippet&order=viewCount&q=comedy&maxResults=10&key=" +BuildConfig.API_KEY ;

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }
}
