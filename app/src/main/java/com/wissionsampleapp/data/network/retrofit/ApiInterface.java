package com.wissionsampleapp.data.network.retrofit;


import com.wissionsampleapp.BuildConfig;
import com.wissionsampleapp.data.network.models.feed.Feeds;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Shivam Sharma on 30-12-2018.
 */

public interface ApiInterface {

   /* @POST("user/login")
    Call<User> manualLogin(@Body UserLoginDetails body);*/

    @GET(ApiEndPoint.ENDPOINT_YOUTUBE_SEARCH)
    Call<Feeds> getFeeds();

    @GET(ApiEndPoint.ENDPOINT_YOUTUBE_ANALYTICS)
    Call<Feeds> getViewCount();

}
