
package com.wissionsampleapp.data.network.models.feed.viewCount;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public class ViewCount {

    @SerializedName("items")
    @Expose
    private List<Item> items = null;


    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public static class Item {

        @SerializedName("statistics")
        @Expose
        private Statistics statistics;

        public Statistics getStatistics() {
            return statistics;
        }

        public void setStatistics(Statistics statistics) {
            this.statistics = statistics;
        }


        public static class Statistics {

            @SerializedName("viewCount")
            @Expose
            private String viewCount;


            public String getViewCount() {
                return viewCount;
            }

            public void setViewCount(String viewCount) {
                this.viewCount = viewCount;
            }

        }
    }
}
