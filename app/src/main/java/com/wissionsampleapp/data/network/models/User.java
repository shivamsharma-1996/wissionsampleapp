package com.wissionsampleapp.data.network.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

//Model class representing a user, will mostly be used to represent current user
@IgnoreExtraProperties
public class User {
    @Expose
    @SerializedName("email")
    private String mEmail;

    @Expose
    @SerializedName("image")
    private String mImage;

    @Expose
    @SerializedName("name")
    private String mName;

    @Expose
    @SerializedName("wission_handle")
    private String mWissionHandle;

    /**
     * This field should be used for storing key(UID) of user, otherwise ignore it
     */
    @Exclude
    private String mKey;

    @PropertyName("email")
    public String getEmail() {
        return mEmail;
    }

    @PropertyName("email")
    public void setEmail(String email) {
        mEmail = email;
    }

    @PropertyName("image")
    public String getImage() {
        return mImage;
    }

    @PropertyName("image")
    public void setImage(String image) {
        mImage = image;
    }

    @PropertyName("name")
    public String getName() {
        return mName;
    }

    @PropertyName("name")
    public void setName(String name) {
        mName = name;
    }

    @PropertyName("wission_handle")
    public String getWissionHandle() {
        return mWissionHandle;
    }

    @PropertyName("wission_handle")
    public void setSlackHandle(String wissionHandle) {
        mWissionHandle = wissionHandle;
    }

    @Exclude
    public String getKey() {
        return mKey;
    }

    @Exclude
    public void setKey(String key) {
        this.mKey = key;
    }

}
