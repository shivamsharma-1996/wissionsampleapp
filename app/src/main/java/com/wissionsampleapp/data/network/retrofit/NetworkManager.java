package com.wissionsampleapp.data.network.retrofit;

import android.util.Log;
import com.wissionsampleapp.utils.Constants;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shivam Sharma on 28-12-2018.
 */

public class NetworkManager {

    private static final String TAG = "Application Handler";

    private static final int READ_TIMEOUT_SECONDS = 15;
    private static final int WRITE_TIMEOUT_SECONDS = 15;
    private static final int CONNECTION_TIMEOUT_SECONDS = 15;

    private static ApiInterface apiInterface;

    public static void initialize() {
        createRetrofitObject();
    }

    private static void createRetrofitObject() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getBaseURL())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiInterface = retrofit.create(ApiInterface.class);
    }

    public static String getBaseURL() {
        return Constants.URL_API;
    }

    public static ApiInterface getApiInterface() {
        if (apiInterface == null) {
            Log.i(TAG, "Api interface is null");
            createRetrofitObject();
        }
        return apiInterface;
    }
}
