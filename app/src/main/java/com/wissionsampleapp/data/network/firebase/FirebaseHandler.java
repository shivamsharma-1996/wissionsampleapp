package com.wissionsampleapp.data.network.firebase;

import com.wissionsampleapp.data.DataManager;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */
public interface FirebaseHandler {

    String REF_USERS_NODE = "users";         //private final

    void loginWithFirebase(String email, String password, DataManager.Callback<Boolean> callback);


    void destroy();

    /**
     * Generic callback interface for passing response to caller.
     * @param <T> Type of expected response
     */
    interface Callback<T> {
        void onReponse(T result);

        void onError();
    }
}