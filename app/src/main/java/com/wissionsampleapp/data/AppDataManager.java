package com.wissionsampleapp.data;

import android.content.Context;
import com.wissionsampleapp.application.AppClass;
import com.wissionsampleapp.data.network.firebase.FirebaseHandler;
import com.wissionsampleapp.data.network.firebase.FirebaseHandlerImpl;
import com.wissionsampleapp.data.prefs.AppPreferencesHelper;
import com.wissionsampleapp.data.prefs.PreferencesHelper;
import com.wissionsampleapp.di.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */
@Singleton
public class AppDataManager implements DataManager {

//    private static AppDataManager INSTANCE = null;

    private PreferencesHelper mPreferences;
    private FirebaseHandler mFirebaseHandler;

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final FirebaseHandler firebaseHandler;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          FirebaseHandler firebaseHandler) {
        this.mContext = context;
        this.mPreferencesHelper = preferencesHelper;
        this.firebaseHandler = firebaseHandler;
    }

//    /*static AppDataManager getInstance() {
//        if (INSTANCE == null) {
//            synchronized (AppDataManager.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = new AppDataManager();
//                }
//            }
//        }
//        return INSTANCE;
//    }
//*/

    @Override
    public void loginWithFirebase(String email, String password, Callback<Boolean> callback) {
        firebaseHandler.loginWithFirebase(email,password, callback);
    }

    @Override
    public void destroy() {
        mPreferences.destroy();
        mFirebaseHandler.destroy();
    }

    /**
     * internal class for converting {@link FirebaseHandler} Callback to {@link DataManager} Callback
     *
     * @param <T> type of response that is expected
     */
    class FirebaseCallback<T> implements FirebaseHandler.Callback<T> {
        DataManager.Callback<T> callback;

        FirebaseCallback(DataManager.Callback<T> callback) {
            this.callback = callback;
        }

        @Override
        public void onReponse(T result) {
            this.callback.onResponse(result);
        }

        @Override
        public void onError() {
            this.callback.onError();
        }
    }
}