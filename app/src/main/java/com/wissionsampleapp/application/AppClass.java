package com.wissionsampleapp.application;

import android.app.Application;
import android.content.Context;
import com.wissionsampleapp.data.DataManager;
import com.wissionsampleapp.di.component.ApplicationComponent;
import com.wissionsampleapp.di.component.DaggerApplicationComponent;
import com.wissionsampleapp.di.module.ApplicationModule;

import javax.inject.Inject;

/**
 * Created by Shivam Sharma on 28-12-2018.
 */

public class AppClass extends Application {

    private static final String TAG = "AppClass";
    @Inject
    DataManager mDataManager;

    private ApplicationComponent mApplicationComponent;

    private static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this.getApplicationContext();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);
    }

    public static Context getAppContext(){
        return mContext;
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}