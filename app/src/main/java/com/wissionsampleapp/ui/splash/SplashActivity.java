package com.wissionsampleapp.ui.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.wissionsampleapp.R;
import com.wissionsampleapp.ui.base.BaseActivity;
import com.wissionsampleapp.ui.dashboard.DashboardActivity;
import com.wissionsampleapp.ui.login.LoginActivity;

import javax.inject.Inject;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public class SplashActivity extends BaseActivity implements SplashMvpView {

    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getActivityComponent().inject(this);

        mPresenter.onAttach(SplashActivity.this); //i.e interface extends an interface
    }

    @Override
    protected void setUpView() {

    }

    @Override
    public void openDashboardActivity() {
        Intent intent = DashboardActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Intent intent = LoginActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.setFirebaseAuthListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.removeFirebaseAuthListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDetach();
    }
}
