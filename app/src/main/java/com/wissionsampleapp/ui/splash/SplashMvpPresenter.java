package com.wissionsampleapp.ui.splash;

import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.ui.base.MvpPresenter;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@PerActivity
public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    //void decideNextActivity();
    void setFirebaseAuthListener();
    void removeFirebaseAuthListener();
}
