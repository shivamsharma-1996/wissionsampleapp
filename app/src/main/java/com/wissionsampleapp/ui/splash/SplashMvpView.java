package com.wissionsampleapp.ui.splash;

import com.wissionsampleapp.ui.base.MvpView;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public interface SplashMvpView extends MvpView {

    void openDashboardActivity();

    void openLoginActivity();

}
