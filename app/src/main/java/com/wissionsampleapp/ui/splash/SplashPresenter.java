package com.wissionsampleapp.ui.splash;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.wissionsampleapp.data.DataManager;
import com.wissionsampleapp.ui.base.BasePresenter;
import com.wissionsampleapp.ui.login.LoginActivity;
import com.wissionsampleapp.ui.login.LoginMvpView;

import java.util.logging.Handler;

import javax.inject.Inject;

/**
 * Created by Shivam Sharma on 31-12-2018.
 */

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private final FirebaseAuth mFirebaseAuth;

    private FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                Log.d(TAG, "User is Signed In");
                getMvpView().openDashboardActivity();
            } else {
                Log.d(TAG, "User is Signed Out");
                getMvpView().openLoginActivity();
            }
        }
    };

    @Inject
    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void setFirebaseAuthListener() {
            mFirebaseAuth.addAuthStateListener(mAuthListener);
            getMvpView().showLoading();
    }

    @Override
    public void removeFirebaseAuthListener() {
            mFirebaseAuth.removeAuthStateListener(mAuthListener);
        getMvpView().hideLoading();
    }

    /*@Override
    public void decideNextActivity() {
        if (getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            getMvpView().openLoginActivity();
        } else {
            getMvpView().openMainActivity();
        }
    }*/
}
