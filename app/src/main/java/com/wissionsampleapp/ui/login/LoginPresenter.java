package com.wissionsampleapp.ui.login;

import android.widget.Toast;

import com.wissionsampleapp.R;
import com.wissionsampleapp.data.DataManager;
import com.wissionsampleapp.ui.base.BasePresenter;
import com.wissionsampleapp.utils.CommonUtils;
import javax.inject.Inject;

import static com.wissionsampleapp.utils.Constants.PASSWORD_LENGTH_CRITERIA;

/**
 * Created by Shivam Sharma on 30-12-2018.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {

    private static final String TAG = "LoginPresenter";

    @Inject
    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void validateCredentials(String email, String password) {
        //validate email and password
        if (email == null || email.isEmpty()) {
            getMvpView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(email)) {
            getMvpView().onError(R.string.invalid_email);
            return;
        }
        if (password == null || password.isEmpty()) {
            getMvpView().onError(R.string.empty_password);
            return;
        }
        /*if (password.length() < PASSWORD_LENGTH_CRITERIA) {
            getMvpView().onError(R.string.short_password);
            return;
        }*/
        getMvpView().showLoading();

        getDataManager().loginWithFirebase(email, password, new DataManager.Callback<Boolean>() {
            @Override
            public void onResponse(Boolean result) {
                getMvpView().hideLoading();

                if (result==true)
                    //save session in pref
                    getMvpView().openDashboardActivity();
                else
                    this.onError();
            }

            @Override
            public void onError() {
                getMvpView().onError(R.string.something_went_wrong);
            }
        });
    }
}
