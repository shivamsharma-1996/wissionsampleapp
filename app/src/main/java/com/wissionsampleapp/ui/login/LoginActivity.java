package com.wissionsampleapp.ui.login;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wissionsampleapp.R;
import com.wissionsampleapp.ui.base.BaseActivity;
import com.wissionsampleapp.ui.dashboard.DashboardActivity;

import javax.inject.Inject;


/**
 * Created by Shivam Sharma on 30-12-2018.
 */

public class LoginActivity extends BaseActivity implements LoginMvpView {

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    private ImageView ivClose;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnLogin;
    private TextView btnSignup;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);
        mPresenter.onAttach(LoginActivity.this);

        setUpView();
    }

    @Override
    protected void setUpView() {
        ivClose = findViewById(R.id.iv_close);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btn_signin);
        btnSignup = findViewById(R.id.tv_signup);

        ivClose.setOnClickListener(v -> this.finish());
        btnLogin.setOnClickListener(v -> onLoginClick());
        btnSignup.setOnClickListener(v -> onSignupClick());

        //there is nothing to populate
    }

    @Override
    public void openDashboardActivity() {
        startActivity(DashboardActivity.getStartIntent(LoginActivity.this));
    }

    @Override
    public void openSigunpActivity() {
        startActivity(SignupActivity.getIntent(LoginActivity.this));
    }

    @Override
    public void onLoginClick() {
        hideKeyboard();
        mPresenter.validateCredentials(etEmail.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void onSignupClick() {
        this.openSigunpActivity();
    }
}
