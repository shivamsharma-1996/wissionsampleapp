package com.wissionsampleapp.ui.login;


import com.wissionsampleapp.ui.base.MvpView;

/**
 * Created by Shivam Sharma on 30-12-2018.
 */

public interface LoginMvpView extends MvpView {

    void openDashboardActivity();

    void openSigunpActivity();

    void onLoginClick();

    void onSignupClick();

}
