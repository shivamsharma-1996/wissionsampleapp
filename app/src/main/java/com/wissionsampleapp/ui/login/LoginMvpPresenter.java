package com.wissionsampleapp.ui.login;


import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.ui.base.MvpPresenter;

/**
 * Created by Shivam Sharma on 30-12-2018.
 */

@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void validateCredentials(String email, String password);

}
