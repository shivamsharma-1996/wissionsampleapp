package com.wissionsampleapp.ui.dashboard;

import android.support.constraint.Group;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wissionsampleapp.R;
import com.wissionsampleapp.data.network.models.feed.Feeds;
import com.wissionsampleapp.data.network.models.feed.Item;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.FeedsViewHolder> {

    List<Item> feedItemList;
    private List<Item> feedItems;

    public DashboardAdapter() {
    }

    public DashboardAdapter(List<Item> feedItemList) {
        this.feedItemList = feedItemList;
    }

    @Override
    public FeedsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View feedView = LayoutInflater.from( parent.getContext()).inflate(R.layout.item_feeds_layout, parent, false);
        return new FeedsViewHolder(feedView);
    }

    @Override
    public void onBindViewHolder(FeedsViewHolder holder, int position) {

        holder.wvFeedVideo.loadData( "<html><body>Video From YouTube<br><iframe width=\\\"100px\\\" height=\\\"100px\\\" src=\\\"https://www.youtube.com/embed/47yJ2XCRLZs\\\" frameborder=\\\"0\\\" allowfullscreen></iframe></body></html>", "text/html" , "utf-8" );

        //  holder.wvFeedVideo.loadData( (feedItemList.get(position).getId()).getVideoId(), "text/html" , "utf-8" );
//        holder.tvTitle.setText((feedItemList.get(position).getSnippet()).getTitle());
//        holder.tvViewCount.setText((feedItemList.get(position).getSnippet()).getTitle());

    }

    @Override
    public int getItemCount() {
            return 10;
    }

    public void setFeedItems(List<Item> feedItems) {
        this.feedItems = feedItems;
    }

    public class FeedsViewHolder extends RecyclerView.ViewHolder{

        WebView wvFeedVideo;
        TextView tvTitle;
        TextView tvViewCount;
        ImageView ivLike;
        ImageView ivComment;
        EditText etComment;
        Button btnPostComment;
        Group groupCommentView;

        public FeedsViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvViewCount = itemView.findViewById(R.id.tv_viewCount);
            ivLike = itemView.findViewById(R.id.iv_like);
            ivComment = itemView.findViewById(R.id.iv_comment);
            etComment = itemView.findViewById(R.id.et_comment);
            btnPostComment = itemView.findViewById(R.id.btn_post);
            groupCommentView = itemView.findViewById(R.id.group_comment_view);

            wvFeedVideo = itemView.findViewById(R.id.wv_feed_video);
            wvFeedVideo.getSettings().setJavaScriptEnabled(true);
            //wvFeedVideo.setWebChromeClient(new WebChromeClient());
            wvFeedVideo.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }
            });
            wvFeedVideo.setWebChromeClient(new WebChromeClient());
        }
    }
}