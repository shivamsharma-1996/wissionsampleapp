package com.wissionsampleapp.ui.dashboard;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.wissionsampleapp.R;
import com.wissionsampleapp.data.network.models.feed.Feeds;
import com.wissionsampleapp.data.network.models.feed.Item;
import com.wissionsampleapp.data.network.retrofit.NetworkManager;
import com.wissionsampleapp.ui.base.BaseActivity;
import com.wissionsampleapp.ui.login.LoginActivity;
import com.wissionsampleapp.ui.login.SignupActivity;
import com.wissionsampleapp.ui.profileSetup.ProfileSetupActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity implements View.OnClickListener{

    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    @Inject
    DashboardAdapter mDashboardAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    ArrayList<Item> feedItemList;

    private RecyclerView mRecyclerView;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, DashboardActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getActivityComponent().inject(this);


        /*appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
                {
                    // Collapsed
                    setTitle("Title To Show");
                }
                else
                {
                    // Expanded
                    setTitle("");
                }
            }
        });*/

        setUpView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dashboard, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_edit_profile){
            startActivity(ProfileSetupActivity.getStartIntent(DashboardActivity.this));
        }
        else if(id == R.id.menu_logout){
            startActivity(LoginActivity.getStartIntent(DashboardActivity.this));

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setUpView() {
        mRecyclerView = findViewById(R.id.rec_feedList);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mDashboardAdapter);

        this.fetchNetworkFeeds();  //TO DO, use here - mPresenter.onViewPrepared();
    }

    private void fetchNetworkFeeds() {
        new Runnable() {
            @Override
            public void run() {
                NetworkManager.getApiInterface()
                        .getFeeds()
                        .enqueue(new Callback<Feeds>() {
                            @Override
                            public void onResponse(Call<Feeds> call, Response<Feeds> response) {
                                if (response.isSuccessful() && response.body() != null && response.body().getItems() != null && response.body().getNextPageToken() != null) {
                                    //networkRecommendations.clear();
                                    feedItemList.addAll(response.body().getItems());
                                    mDashboardAdapter.setFeedItems(feedItemList);
                                }
                            }

                            @Override
                            public void onFailure(Call<Feeds> call, Throwable t) {

                            }
                        });
            }
        }.run();
    }

    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (! isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (! isDeviceOnline()) {
            //mOutputText.setText("No network connection available.");
        } else {
           // new MakeRequestTask(mCredential).execute();
        }
    }


    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     * @return true if Google Play Services is available and up to
     *     date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                DashboardActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * An asynchronous task that handles the YouTube Data API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
//    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
//        private com.google.api.services.youtube.YouTube mService = null;
//        private Exception mLastError = null;
//
//        MakeRequestTask(GoogleAccountCredential credential) {
//            HttpTransport transport = AndroidHttp.newCompatibleTransport();
//            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
//            mService = new com.google.api.services.youtube.YouTube.Builder(
//                    transport, jsonFactory, credential)
//                    .setApplicationName("YouTube Data API Android Quickstart")
//                    .build();
//        }
//
//        /**
//         * Background task to call YouTube Data API.
//         * @param params no parameters needed for this task.
//         */
//        @Override
//        protected List<String> doInBackground(Void... params) {
//            https://www.googleapis.com/youtube/v3/search?part=snippet&order=viewCount&q=comedy&maxResults=10&key=AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM"
//
//            try {
//                URL url = new URL(
//
//                        "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&order=viewCount&q=stand%20up%20comedy&maxResults=10&key=AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM");
//                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                try {
//                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                    //readStream(in);
//                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
//                    StringBuilder total = new StringBuilder();
//                    for (String line; (line = r.readLine()) != null; ) {
//                        total.append(line).append('\n');
//                    }
//                    Log.i("DASHBBOARDACTIVIYT", String.valueOf(total));
//                } finally {
//                    urlConnection.disconnect();
//                }
//
//                return getDataFromApi();
//            } catch (Exception e) {
//                mLastError = e;
//                cancel(true);
//                return null;
//            }
//        }
//
//        /**
//         * Fetch information about the "GoogleDevelopers" YouTube channel.
//         * @return List of Strings containing information about the channel.
//         * @throws IOException
//         */
//        private List<String> getDataFromApi() throws IOException {
//            // Get a list of up to 10 files.
//            List<String> channelInfo = new ArrayList<String>();
//            ChannelListResponse result = mService.channels().list("snippet,contentDetails,statistics")
//                    .setForUsername("GoogleDevelopers")
//                    .execute();
//            List<Channel> channels = result.getItems();
//            if (channels != null) {
//                Channel channel = channels.get(0);
//                channelInfo.add("This channel's ID is " + channel.getId() + ". " +
//                        "Its title is '" + channel.getSnippet().getTitle() + ", " +
//                        "and it has " + channel.getStatistics().getViewCount() + " views.");
//            }
//            return channelInfo;
//        }
//
//
//        @Override
//        protected void onPreExecute() {
//            mOutputText.setText("");
//            mProgress.show();
//        }
//
//        @Override
//        protected void onPostExecute(List<String> output) {
//            mProgress.hide();
//            if (output == null || output.size() == 0) {
//                mOutputText.setText("No results returned.");
//            } else {
//                output.add(0, "Data retrieved using the YouTube Data API:");
//                mOutputText.setText(TextUtils.join("\n", output));
//            }
//        }
//
//        @Override
//        protected void onCancelled() {
//            mProgress.hide();
//            if (mLastError != null) {
//                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
//                    showGooglePlayServicesAvailabilityErrorDialog(
//                            ((GooglePlayServicesAvailabilityIOException) mLastError)
//                                    .getConnectionStatusCode());
//                } else if (mLastError instanceof UserRecoverableAuthIOException) {
//                    startActivityForResult(
//                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
//                            DashboardActivity.REQUEST_AUTHORIZATION);
//                } else {
//                    mOutputText.setText("The following error occurred:\n"
//                            + mLastError.getMessage());
//                }
//            } else {
//                mOutputText.setText("Request cancelled.");
//            }
//        }
//    }
}