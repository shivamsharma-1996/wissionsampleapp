package com.wissionsampleapp.ui.dashboard;

import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.ui.base.MvpPresenter;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@PerActivity
public interface DashboardMvpPresenter<V extends DashboardMvpView> extends MvpPresenter<V> {


}
