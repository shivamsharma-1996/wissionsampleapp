package com.wissionsampleapp.ui.profileSetup;

import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.ui.base.MvpPresenter;
import com.wissionsampleapp.ui.splash.SplashMvpView;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@PerActivity
public interface ProfileSetupMvpPresenter<V extends ProfileSetupMvpView> extends MvpPresenter<V> {


}
