package com.wissionsampleapp.ui.base;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);  // attach instance of  activity-specific mvpView in onCreate();

    void onDetach();           // detach the attached instance of that mvpView in onDestroy();

    //void handleApiError(ANError error);

    //void setUserAsLoggedOut();
}
