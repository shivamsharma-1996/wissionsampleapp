package com.wissionsampleapp.utils;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public final class AppConstants {

    public static final String PREF_NAME = "WISSION_PREF";
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}


//pageToken
//AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM
//https://www.googleapis.com/youtube/v3/search?part=snippet&order=viewCount&q=comedy&maxResults=10&key=AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM




//correct
//https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoEmbeddable=true&order=viewCount&q=stand up comedy&maxResults=10&key=AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM


//for analytics
//https://www.googleapis.com/youtube/v3/vide os?part=statistics&id=eIVxPRqgtD4&key=AIzaSyBV86-ZcTbI9-R7X3As1mgKc4yRiyYo5qM