package com.wissionsampleapp.utils;

/**
 * Created by Shivam Sharma on 31-12-2018.
 */
public class Constants {

    public final static String URL_API = "https://www.googleapis.com/youtube/v3/";
    public final static int PASSWORD_LENGTH_CRITERIA = 6;

}
