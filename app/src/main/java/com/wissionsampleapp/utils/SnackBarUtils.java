package com.wissionsampleapp.utils;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

public class SnackBarUtils {

    public static void showSnackbar(View view, final Context context, @DrawableRes int drawable, @StringRes int messageText,
                                    final int actionText, View.OnClickListener onClickListener, @ColorRes int actionColor) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Snackbar snackBar = Snackbar.make(view, messageText, Snackbar.LENGTH_LONG);
        snackBar.setAction(actionText,onClickListener).
                setActionTextColor(ContextCompat.getColor(context, actionColor)).
                show();
    }

}
