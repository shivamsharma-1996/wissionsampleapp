package com.wissionsampleapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import com.wissionsampleapp.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Shivam Sharma on 30-12-2018.
 */

public class CommonUtils {

    private static final String TAG = "CommonUtils";

    public static Dialog showLoadingDialog(Context context) {
        Dialog mLoadingLayout = new Dialog(context);
            mLoadingLayout.setContentView(R.layout.dialog_loading_layout);
            if (mLoadingLayout.getWindow() != null) {
                mLoadingLayout.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        }
        mLoadingLayout.setCancelable(false);
        mLoadingLayout.setCanceledOnTouchOutside(false);
        mLoadingLayout.show();
        return mLoadingLayout;
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
