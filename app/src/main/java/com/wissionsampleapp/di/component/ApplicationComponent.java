package com.wissionsampleapp.di.component;

import android.app.Application;
import android.content.Context;

import com.wissionsampleapp.application.AppClass;
import com.wissionsampleapp.data.DataManager;
import com.wissionsampleapp.di.ApplicationContext;
import com.wissionsampleapp.di.module.ApplicationModule;
import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(AppClass app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}