package com.wissionsampleapp.di.component;

import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.di.module.ActivityModule;
import com.wissionsampleapp.ui.dashboard.DashboardActivity;
import com.wissionsampleapp.ui.login.LoginActivity;
import com.wissionsampleapp.ui.splash.SplashActivity;
import dagger.Component;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LoginActivity activity);

    void inject(SplashActivity activity);

    void inject(DashboardActivity activity);

}
