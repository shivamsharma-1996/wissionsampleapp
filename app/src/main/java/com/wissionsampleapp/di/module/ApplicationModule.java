package com.wissionsampleapp.di.module;

import android.app.Application;
import android.content.Context;

import com.wissionsampleapp.data.AppDataManager;
import com.wissionsampleapp.data.DataManager;
import com.wissionsampleapp.data.network.firebase.FirebaseHandler;
import com.wissionsampleapp.data.network.firebase.FirebaseHandlerImpl;
import com.wissionsampleapp.data.prefs.AppPreferencesHelper;
import com.wissionsampleapp.data.prefs.PreferencesHelper;
import com.wissionsampleapp.di.ApplicationContext;
import com.wissionsampleapp.di.PreferenceInfo;
import com.wissionsampleapp.utils.AppConstants;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    FirebaseHandler provideFirebaseHandler(FirebaseHandlerImpl appApiHelper) {
        return appApiHelper;
    }

}
