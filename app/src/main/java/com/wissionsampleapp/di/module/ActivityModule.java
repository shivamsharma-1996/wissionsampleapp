package com.wissionsampleapp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.wissionsampleapp.data.network.models.feed.Feeds;
import com.wissionsampleapp.data.network.models.feed.Item;
import com.wissionsampleapp.di.ActivityContext;
import com.wissionsampleapp.di.PerActivity;
import com.wissionsampleapp.ui.dashboard.DashboardAdapter;
import com.wissionsampleapp.ui.login.LoginMvpPresenter;
import com.wissionsampleapp.ui.login.LoginMvpView;
import com.wissionsampleapp.ui.login.LoginPresenter;
import com.wissionsampleapp.ui.splash.SplashMvpPresenter;
import com.wissionsampleapp.ui.splash.SplashMvpView;
import com.wissionsampleapp.ui.splash.SplashPresenter;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Shivam Sharma on 29-12-2018.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }



    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    DashboardAdapter provideDashboardAdapter() {
        return new DashboardAdapter(new ArrayList<Item>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @PerActivity
    ArrayList<Item> providesFeedItemList() {
        return new ArrayList<>();
    }

}
